//import com.typesafe.sbt.SbtStartScript

name := "ozone-extensions"

version := "0.0.1-SNAPSHOT"

organization := "io.ozone"

scalaVersion := "2.10.3"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8", "-feature")

lazy val ozone_extensions = project.in(file("."))

resolvers ++= Seq(
  "spray repo" at "http://repo.spray.io/",
  "rug.release" at "http://sm4all-project.eu/nexus/content/repositories/rug.release",
  "apache snapshots" at "http://repository.apache.org/snapshots",
  "maven central" at "http://repo1.maven.org/maven2",
  "oss sonatype" at "http://oss.sonatype.org/content/repositories/releases",
  "twitter" at "http://maven.twttr.com/"
)

testOptions in Test += Tests.Argument("-oD")

val akkaV = "2.2.3"
val sprayV = "1.2.0"
val jcloudsV = "1.7.1"
val graphV = "1.6.0"

libraryDependencies ++= Seq(
  "io.spray"            %   "spray-can"       % sprayV force(),
  "io.spray"            %   "spray-routing"   % sprayV force(),
  "com.typesafe.akka"   %%  "akka-actor"      % akkaV force(),
  "com.typesafe.akka"   %%  "akka-slf4j"      % akkaV force()
//  "io.spray"            %%  "spray-json"      % "1.2.5" force()
)

//RuG governance compliance
libraryDependencies ++= Seq(
    "org.rugco"           %   "server"          % "1.4.2",
    "org.rugco.opt"       %   "scheduler"       % "0.3.1",
    "com.google.protobuf" %   "protobuf-java"   % "2.4.1" force()
)

//ZooKeeper
libraryDependencies ++= Seq(
  "com.twitter" %% "util-zk"         % "6.12.1",
  "com.twitter" %% "util-zk-common"  % "6.12.1"
)

//Extra
libraryDependencies ++= Seq(
  "org.scala-lang"               %  "scala-library"      % "2.10.3",
  "org.apache.jclouds"           %  "jclouds-all"        % jcloudsV,
  "org.apache.jclouds.driver"    %  "jclouds-slf4j"      % jcloudsV,
  "org.apache.jclouds.driver"    %  "jclouds-sshj"       % jcloudsV,
  "org.apache.jclouds.driver"    %  "jclouds-enterprise" % jcloudsV,
  "com.assembla.scala-incubator" %% "graph-core"         % graphV,
  "com.assembla.scala-incubator" %% "graph-dot"          % graphV,
  "com.typesafe"                 %  "config"             % "1.0.1",
  "ch.qos.logback"               %  "logback-classic"    % "1.0.13",
  "com.github.nscala-time"       %% "nscala-time"        % "0.8.0"
)

//Testing
libraryDependencies ++= Seq(
  "com.typesafe.akka"   %%  "akka-testkit"    % akkaV    % "test" force(),
  "io.spray"            %   "spray-testkit"   % sprayV   % "test" force(),
  "org.specs2"          %%  "specs2"          % "2.0"    % "test",
  "org.scalatest"       %%  "scalatest"       % "2.0.M7" % "test",
  "junit"               %   "junit"           % "4.11"   % "test"
)
